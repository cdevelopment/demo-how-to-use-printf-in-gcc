/*==============================================================================
**                                 
**                      Proprietary - Copyright (C) 2017
**------------------------------------------------------------------------------
** Supported MCUs      : STM32F
** Supported Compilers : GCC
**------------------------------------------------------------------------------
** File name           : SerialDbg.c
**
** Project name        : Demo How to use printf function in GCC for debugging
**
**
** Summary: www.cmq.vn - Embedded Sytem Articles, IOT Technology
**
**= History ====================================================================
** 1.0  11/16/2017  CMQ Team
** - Creation
==============================================================================*/

/******************************************************************************/
/* INCLUSIONS                                                                 */
/******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "SerialDbg.h"
#include "mcu.h"
/******************************************************************************/
/* DEFINITION OF CONST                                                        */
/******************************************************************************/

/******************************************************************************/
/* DECLARATION OF VARIABLES OF MODULE                                         */
/* Declaration of local variables shall have the specifier STATIC             */
/******************************************************************************/

/* Definition for USART1 clock resources */
#define USART1_CLK_ENABLE()              __USART1_CLK_ENABLE()
#define USART1_RX_GPIO_CLK_ENABLE()      __GPIOB_CLK_ENABLE()
#define USART1_TX_GPIO_CLK_ENABLE()      __GPIOA_CLK_ENABLE()

#define USART1_FORCE_RESET()             __USART1_FORCE_RESET()
#define USART1_RELEASE_RESET()           __USART1_RELEASE_RESET()

/* Definition for USARTx Pins */
#define USART1_TX_PIN                    GPIO_PIN_9
#define USART1_TX_GPIO_PORT              GPIOA
#define USART1_TX_AF                     GPIO_AF7_USART1

#if(GW_PLATFORM == PLT_DISCOVERY)
    #define USART1_RX_PIN                    GPIO_PIN_7
    #define USART1_RX_GPIO_PORT              GPIOB
#elif(GW_PLATFORM == PLT_PRODUCT_BOARD)
    #define USART1_RX_PIN                    GPIO_PIN_10
    #define USART1_RX_GPIO_PORT              GPIOA
#endif

#define USART1_RX_AF                     GPIO_AF7_USART1

UART_HandleTypeDef UartHandle;
/******************************************************************************/
/* DECLARATION OF LOCAL FUNCTIONS (Internal Functions)                        */
/* Declaration of local functions shall have the specifier STATIC             */
/******************************************************************************/

/******************************************************************************/
/* DEFINITION OF GLOBAL FUNCTIONS (APIs, Callbacks and MainFunction(s))       */
/******************************************************************************/
void Dbg_Init(void)
{
    GPIO_InitTypeDef  GPIO_InitStruct;

    /* Enable GPIO TX/RX clock */
    USART1_TX_GPIO_CLK_ENABLE();
    USART1_RX_GPIO_CLK_ENABLE();

    /* Enable USARTx clock */
    USART1_CLK_ENABLE(); 

    /* UART TX GPIO pin configuration  */
    GPIO_InitStruct.Pin       = USART1_TX_PIN;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull      = GPIO_PULLUP;
    GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = USART1_TX_AF;
    HAL_GPIO_Init(USART1_TX_GPIO_PORT, &GPIO_InitStruct);

    /* UART RX GPIO pin configuration  */
    GPIO_InitStruct.Pin = USART1_RX_PIN;
    GPIO_InitStruct.Alternate = USART1_RX_AF;
    HAL_GPIO_Init(USART1_RX_GPIO_PORT, &GPIO_InitStruct);

    UartHandle.Instance        = USART1;
    UartHandle.Init.BaudRate   = 115200;
    UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    UartHandle.Init.Mode       = UART_MODE_TX_RX;
    UartHandle.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
    UartHandle.Init.StopBits   = UART_STOPBITS_1;
    UartHandle.Init.Parity     = UART_PARITY_NONE;

    HAL_UART_DeInit(&UartHandle);
    HAL_UART_Init(&UartHandle);

    /* NVIC for USART */
    /*HAL_NVIC_SetPriority(USART1_IRQn, 0, 1);
    HAL_NVIC_EnableIRQ(USART1_IRQn);*/
}

void Dbg_PutChar(char c)
{
    uint32_t timeout = 5000;
    /* wait for last character send out */
    while(!(UartHandle.Instance->ISR & UART_FLAG_TXE) && timeout--)
    {
        /* do nothing */
    }
    /* send new character if no error */
    if(timeout)
    {
        UartHandle.Instance->TDR = (uint8_t)c;
    }

}

char Dbg_GetChar(void)
{
    char ch = 0x00;
    if (UartHandle.Instance->ISR & UART_FLAG_RXNE)
    {
        ch = UartHandle.Instance->RDR & 0xff;
    }

    return ch;
}

/*
 * _write
 * Retarget SLOGF function with USART IP
 */
int _write(int file, char *ptr, int len)
{
    int tx_len = len;
    while(tx_len--)
    {
        Dbg_PutChar(*ptr++);
    }
    return len;
}

/*
 * _read
 * Retarget scanf function with USART IP
 */
int _read(int file, char *ptr, int len)
{
    int rx_len = 0x00;
    while(rx_len < len)
    {
        *ptr++ = Dbg_GetChar();
    }
    return rx_len;
}
/******************************************************************************/
/* DEFINITION OF LOCAL FUNCTIONS (Internal Functions)                         */
/* Declaration of local functions shall have the specifier STATIC             */
/******************************************************************************/


/************************* End of File ****************************************/